﻿using NServiceBus;
using NServiceBus.Logging;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using WebApplication.StoreOrder;

namespace WebApplication.App_Start
{
    public class MyMessageHandler : IHandleMessages<ContactMessage>
    {
        static ILog log = LogManager.GetLogger<MyMessageHandler>();

        public Task Handle(ContactMessage message, IMessageHandlerContext context)
        {
            log.Info("Hello from MyHandler");
            var json = new JavaScriptSerializer().Serialize(message);
            log.Info("Message Recieved: " + json);

            IOrderStorageService client = new StoreOrder.OrderStorageServiceClient();
            client.SaveOrder(new StoreOrder.Order { OrderMessage = message });

            return Task.CompletedTask;
        }
    }
}