﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using NServiceBus;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        IEndpointInstance endpoint;

        public HomeController(IEndpointInstance endpoint)
        {
            this.endpoint = endpoint;
        }
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Contact(ContactModel c)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var message = new ContactMessage()
                    {
                        Comment = c.Comment,
                        Email = c.Email,
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        TransectionId = Guid.NewGuid(),
                        Type = MessageType.Storage
                    };
                    
                    await endpoint.Send("Samples.Mvc.Endpoint", message).ConfigureAwait(false);
                   
                    return View("Success");
                }
                catch (Exception ex)
                {
                    return View("Error");
                }
            }
            return View();
        }

        //MailMessage msg = new MailMessage();
        //SmtpClient smtp = new SmtpClient();
        //MailAddress from = new MailAddress(c.Email.ToString());
        //StringBuilder sb = new StringBuilder();
        //msg.IsBodyHtml = false;
        //smtp.Host = "mail.yourdomain.com";
        //smtp.Port = 25;
        //msg.To.Add("youremail@email.com");
        //msg.From = from;
        //msg.Subject = "Contact Us";
        //sb.Append("First name: " + c.FirstName);
        //sb.Append(Environment.NewLine);
        //sb.Append("Last name: " + c.LastName);
        //sb.Append(Environment.NewLine);
        //sb.Append("Email: " + c.Email);
        //sb.Append(Environment.NewLine);
        //sb.Append("Comments: " + c.Comment);
        //msg.Body = sb.ToString();
        //smtp.Send(msg);
        //msg.Dispose();
    }
}