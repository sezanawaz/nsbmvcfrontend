﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using NServiceBus.Features;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        IEndpointInstance endpoint;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigureServiceBus();
        }

        private async void ConfigureServiceBus()
        {
            var endpointConfiguration = new EndpointConfiguration("Samples.Mvc.Endpoint");

            endpointConfiguration.UseSerialization<JsonSerializer>();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.UseTransport<MsmqTransport>();
            endpointConfiguration.SendFailedMessagesTo("Samples.Mvc.Endpoint.Error");
            endpointConfiguration.AuditProcessedMessagesTo("Samples.Mvc.Endpoint.Audit");

            var recoverability = endpointConfiguration.Recoverability();
            recoverability.Delayed(
                delayed =>
                {
                    delayed.NumberOfRetries(2);
                    delayed.TimeIncrease(TimeSpan.FromSeconds(2));
                });
            endpointConfiguration.EnableInstallers();
            //endpoint = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();
            endpoint = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);

            var mvcContainerBuilder = new ContainerBuilder();
            mvcContainerBuilder.RegisterInstance(endpoint);
            // Register MVC controllers.
            mvcContainerBuilder.RegisterControllers(typeof(MvcApplication).Assembly);
            var mvcContainer = mvcContainerBuilder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(mvcContainer));
        }

    }
}
