﻿
using NServiceBus;
using NServiceBus.Logging;
using Shared;
using System;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MessageHandler
{
    public class ContactMessageHandler: IHandleMessages<ContactMessage>
    {
        static ILog log = LogManager.GetLogger<ContactMessageHandler>();

        public Task Handle(ContactMessage message, IMessageHandlerContext context)
        {
            log.Info("Hello from MyHandler");
            var json = new JavaScriptSerializer().Serialize(message);
            log.Info("Message Recieved: " + json);
            //if (message.OrderId != null)
            //{
            //    throw new Exception("Error in Message");            
            //}
            return Task.CompletedTask;
        }
    }
}
