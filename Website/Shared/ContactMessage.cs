﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    [DataContract]
    public class ContactMessage: IMessage
    {
        [DataMember]
        public Guid TransectionId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public MessageType Type { get; set; }
    }

    public enum MessageType
    {
        Storage,
        Reporting,
        CRM
    }
}
